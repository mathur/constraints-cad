# Constraint Synthesis for Parametric CAD (code and experiments from the paper).
The aim of this project is to synthesize constraints to CAD parameters.

## Installation instructions
This project requires Python version 3.x (tested with Python 3.6, 3.7, and 3.8 on Windows).

We provide instructions for Anaconda, but individual packages can also be installed separately.
The required packages are:

- [PythonOCC version 7.4.1](https://github.com/tpaviot/pythonocc-core/releases/tag/7.4.1)
- PuLP
- [CadQuery (version 2.0)](https://github.com/CadQuery/cadquery/releases/tag/2.0)  
- GLPK --> typically installed with PuLP

The installation instructions for Anaconda users are given.
Run the following statements (with admin privileges where applicable).

For CadQuery:
There is an issue with the new version of CadQuery and PythonOCC, so we provide a modified CadQuery 2.0 with our project.
No separate install needed.

You will also need PythonOCC version 7.4.1:

```buildoutcfg pythonOCC
conda install -c conda-forge pythonocc-core=7.4.1
```
You can also try other [channels] (https://anaconda.org/search?q=pythonocc-core).
Please install version 7.4.1

You will need PuLP for LP based synthesis:
```buildoutcfg PuLP
conda install -c conda-forge pulp
```

If you don't already have it, you will need sklearn
```buildoutcfg scikit-learn
conda install -c anaconda scikit-learn
```

You may need to install numpy as well, if you do not have it already.

## Project scripts
In the main folder, there are the following files:
- `checker.py`: This is the script you need to run to get constraints.
- `prog_representation.py`: This is the script that keeps track of the current CAD operation, whether the previous one succeeds,
what the current set of constraints are, etc.
- `constraints.py`: This script maintains, uses, and builds constraints
- `lp.py`: Contains LP-specific functions. For LP-based synthesis, this is the most important script, containing the
calls to solver, construction of hypotheses, and their validation/refutation.

## Examples
Examples presented in the paper are taken from the [Fusion 360 Segmentation Dataset](https://github.com/AutodeskAILab/Fusion360GalleryDataset).
- The re-designed versions in CadQuery are at `Examples\Fusion360`.
The names of the designs correspond to the name of the object in the dataset.
- Images of the same name provide a snapshot of how the design looks with original parameter values.

## Usage
```commandline
python checker.py <CAD program> <sampling mode> <num_args> <threshold / 8>
```
For example, to run Examples\Fusion360\16550_e88d6986_0.py, which takes 3 parameters for a max. of 40 * 8 steps per 
dynamically analysed param, we use:
```commandline
python checker.py Examples.Fusion360.16550_e88d6986_0 stateful_lp 3 40
```
This runs the constraint synthesis technique on the specified example, and returns the results in the console.
Or, to test with 1000 naive random samples -> contraint synthesis -> constrained sampling for 1000 samples:
```commandline
python checker.py Examples.Fusion360.16550_e88d6986_0 automated_lp 3 40
```
The results get stored in the main folder with the name of the file, and ".info" extension.

## Interpreting the result
The following are some examples, and how to interpret them.
We consider the parameters to be in the list P.

- P[0] takes values in (0, inf)
```
0 UNB 1.0 * 
```

- P[2] < 0.999111 * P[0]
```
2 LT 0.999111 * 0
```

- P[9] < 0.499299 * min(P[7], P[8])
```
9 LT 0.499299 * min('7', '8')
```

-  P[2] < min(0.998589 * P[1], 0.500182 * P[0])
```
2 LT [0.998589, 0.500182] * min_2c('1', '0')
```

- P[3] < min(0.502534 * P[1], 0.250099 * (P[0] - P[2]))
```
3 LT [0.502534, 0.250099] * min_2c('1', 0 - 2)
```

## Annotating CadQuery designs
All designs in the experiments folder are already annotated.
We have not implemented an interpreter for CadQuery, and there annotation is needed for the technique to be able to 
successfully use CadQuery designs.

The already included designs can be used to get an idea of how the annotation needs to be done.

In general, we need to know when a new operation is being done, and the type of operation, as well as the parameters 
involved in the operation.
A reference of the previously used / created object is also needed (as this undergoes validity checks).


## Synthesis modes
There are other synthesis modes included in the project. Using these is not recommended.

Stateful sort mode, which uses extended predicates and gets linear coefficients using sorting (Works, not recommended)
```commandline
python checker.py <CAD program> stateful_sort <num_args> <threshold>
```
Stateful sort singular mode, which uses the same procedure as before, but only varies one parameter at a time. (Does not work, not recommended)
```commandline
python checker.py <CAD program> stateful_sort <num_args> <threshold>
```
Stateful SVM mode, which uses an SVM for classification, along with some state information (Works, not recommended)
```commandline
python checker <CAD program> stateful_svm <num_args> <threshold>
```
Hybrid mode, which uses a stateful SVM and sort based mode together (Works, not recommended)
```commandline
python checker <CAD program> sort_svm_hybrid <num_args> <threshold>
```

## Notes
If installing manually, please be careful of the version being installed. This is important!
The original repository of the project is [here](https://gitlab.mpi-sws.org/mathur/constraintcheck).
