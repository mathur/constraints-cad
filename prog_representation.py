import random
import time
import traceback
import constraints
import lp
# Counting number of solids using Python OCC
from OCC.Core.TopExp import TopExp_Explorer
from OCC.Core.BRepCheck import BRepCheck_Analyzer

from OCC.Core.gp import gp_Pnt
from OCC.Core.BRepClass import BRepClass_FClassifier, BRepClass_FaceExplorer
from OCC.Core.TopAbs import *
from OCC.Core.TopoDS import *
from OCC.Core.BRep import BRep_Tool
# To project vertex on 2D surface
from OCC.Core.ShapeAnalysis import ShapeAnalysis_Surface


class prog_rep:
    """
    Holds a representation of the CAD program,
    especially during its run iteratively over random arguments.
    """
    """
    Modes hold information about the mode the representation is in:
    INIT: has just been initialized
    CREATE: will create a shape
    CSKHOLE: will do counter-sink hole
    CBORE: will do counter-bored hole
    HOLE: will drill a hole
    FILLET: will do a fillet
    CHAMFER: will do a chamfer
    """
    def __init__(self, num_vars, sort_mode=False, min_samples=None, max_samples=None, onebyone=False, svm_mode=False,
                 lp_mode=False, analyze_mode=None, out_file=None):
        """
        Sets up the object representing the program
        :param num_vars: Number of params in the program
        :param onebyone: Change parameter values one by one
        :param svm_mode: Is the stateful svm mode active?
        """
        # Number of iteration (increments after sampling)
        self.iter_num = 0
        # Number of iterations passing
        self.iter_passing = 0
        # The num of args
        self.num_vars = num_vars
        # Random values for the first iteration
        self.args = []
        # All restrictions obtained so far.
        # Restrictions in the current iteration
        self.restrictions_curr = []
        # Consolidated restrictions (these are dictionaries: arg_num-> [restriction1, restriction2 ...])
        # The regression sampling is being done to test which arg_num
        self.curr_arg_num_test = None  # The current arg_num whose restriction is being tested
        self.restrictions_consolidated_may = dict()
        self.restrictions_consolidated_must = dict()
        # Previous snapshot tested in the sampling procedure
        self.prev_restr_snapshot = None
        # Current mode of the program
        self.mode = "INITIALIZED"
        # The intermediate representation being used by the current mode
        self.intermediate = None
        # The argument indices activated by the program.
        # Each elem is a list of elements used at successive line numbers. Initially empty list.
        self.arg_indices_activated = []
        # Compile all arg_nums activated in a flat list
        self.arg_indices_activated_flat = []
        # All variable values tried
        self.vars_all = []

        # Object used previously (Has to be solid, bounding box).
        self.prev_obj = None

        # Pass/fail result of all values tried
        self.curr_it_pass = True
        self.result_all = []

        # List of restrictions that are removed, and needn't be added again
        self.removed_restr = []
        # Structure which stores the sorted predicate values and corresponding results
        self.sorted_pred_results = dict()  # dict of dictionaries (restr->(ordered var vals-> result))

        # List of unresolved args. These are typically args used in 2D operations.
        self.unresolved_args = []

        # Alt mode currently (is there another object being used / built in parallel)
        self.alt_mode = None

        # Timing of the constraint synthesis
        self.time_start = time.time()

        # Sort mode activated or not (The idea is that instead of using dependent predicates consisting of individual
        # variables, additional predicates composite of many variables are added. A sort is used to see if they are
        # uniform. If not, the predicates are dropped.)
        self.sort_mode_active = sort_mode
        constraints.sort_mode_active = sort_mode
        # If the min_samples is not given, use the default in constraints, else, set the given value here.
        if min_samples is not None:
            constraints.sort_min_samples = min_samples
            self.min_samples = min_samples

        if max_samples is not None:
            self.max_samples = max_samples

        # Sample vars one by one. This works by building conjuncts one by one as well.
        if onebyone:
            self.state_onebyone = state_onebyone(p_rep=self)
        else:
            self.state_onebyone = None

        if svm_mode:
            self.state_svm = constraints.state_svm(p_rep=self)
        else:
            self.state_svm = None

        if lp_mode:
            # Initialize LP mode structure
            self.state_lp = lp.state_lp(p_rep=self)

        else:
            self.state_lp = None

        # Set analyze_mode status (0: No ajustment, 1:dynamic adjustment of some params via check_update_var)
        self.analyze_mode = analyze_mode

        # If output to a file desired, the file object
        self.out_file = out_file


    def var_gen_bkg_end(self):
        """
        Reset the variables at the end of each variable generation
        """
        self.vars_all.append(self.args)

        # Reset the currently activated arguments
        self.arg_indices_activated = []
        self.restrictions_curr = []
        # Set the curr it to pass (if it fails, this would be reset).
        self.curr_it_pass = True
        # Reset vars in constraints
        constraints.args_offset = []
        constraints.args_create = []
        constraints.args_alt = []
        constraints.args_cut = []

    def var_gen_bkg(self):
        self.iter_num = self.iter_num + 1
        print("Start of iteration", self.iter_num)

    def analyze_clear_counting(self):
        # Used to test how good/bad the result is by doing many samples with knowledge of result, reporting pass %
        self.iter_num = 0
        self.iter_passing = 0

    def generate_vars_stateful(self):
        """
        Generate new values for params in the prog
        :return:
        """
        self.var_gen_bkg()
        # If it is the first iteration, set all args to random values
        if self.iter_num == 0:
            # As default set random values
            self.args = []
            for i in range(self.num_vars):
                self.args.append(random.uniform(constraints.low_val, constraints.inf))

        else:
            # If restrictions are available, use barrier function
            constraints.barrier_func_sampling(prog_rep=self)
        self.var_gen_bkg_end()

    def generate_vars_lp(self):
        """
        Generation of vars for a new iteration in the LP mode. Implemented in class state_lp
        :return:
        """
        self.var_gen_bkg()
        self.args = self.state_lp.generate_vars()
        self.var_gen_bkg_end()

    def generate_vars_stateful_singular(self):
        """
        Generate new values for params in the prog (stateful singular mode).
        The singular mode fixes all variable values, and changes only one at a time.
        :return:
        """
        self.var_gen_bkg()

        # If dictionaries are  empty (first iteration probs), set random vals to all occurring vars
        if not self.restrictions_consolidated_may and not self.restrictions_consolidated_must:
            # No restrictions to work with right now. In other modes, this may be initialisation.
            pass

        # Use singular mode class to create a new sample
        self.state_onebyone.new_sample()

        # Reset / update other internal variables
        self.var_gen_bkg_end()

    def generate_vars_stateful_svm(self):
        """
        Generate new values for params in the prog
        :return:
        """
        self.var_gen_bkg()

        # Set random values to all variables involved
        self.args = []

        # Call the generate vars method from the stateful svm class
        self.state_svm.generate_new_vars()

        self.var_gen_bkg_end()

    def generate_vars_hybrid(self):
        """
        Generate vars for a new iteration in the hybrid mode.
        We check Must restrictions first, then SVM done.
        :return:
        """
        self.var_gen_bkg()

        # Set random values to all variables involved
        self.args = []
        # If it is the first iteration, set all args to random values
        if self.iter_num == 0:
            # As default set random values
            self.args = []
            for i in range(self.num_vars):
                self.args.append(random.uniform(constraints.low_val, constraints.inf))

        else:
            # Call the generate vars method from the stateful svm class
            self.state_svm.generate_new_vars()
            # If restrictions are available, use barrier function
            constraints.barrier_func_sampling(prog_rep=self, hybrid=True)

        self.var_gen_bkg_end()

    def generate_vars_naive(self):
        """
        Generate new values for params in the prog
        :return:
        """
        self.var_gen_bkg()

        self.args = []
        for i in range(self.num_vars):
            self.args.append(random.uniform(constraints.low_val, constraints.inf))

        self.var_gen_bkg_end()

    def check(self, obj, final=False):
        # TODO: There is an overload of checks. Bounding Box or Validity check may be removed in the future
        # Don't do checks for some operations
        if self.mode not in ["OFFSET", "VERT_ON"]:
            obj.objects[0].BoundingBox()
            # B-rep validity check
            validity_brep = BRepCheck_Analyzer(obj.objects[0].wrapped)
            # print("OCCT validity:", validity_brep.IsValid())
            assert validity_brep.IsValid()
            # Get the number of solids in the obj
            solid_explorer = TopExp_Explorer(obj.objects[0].wrapped, TopAbs_SOLID)
            solid_count = 0
            while solid_explorer.More():
                solid_count = solid_count + 1
                solid_explorer.Next()

            # Assert that there is 1 solid
            if solid_count > 0:
                assert(solid_count == 1)
            else:
                # There are no solids, maybe this object was 2D (i.e. check for wire)?
                wire_explorer = TopExp_Explorer(obj.objects[0].wrapped, TopAbs_WIRE)
                wire_count = 0
                while wire_explorer.More():
                    wire_count = wire_count + 1
                    wire_explorer.Next()
                # There should be just 1 wire
                assert(wire_count == 1)

                # Get vertices in the wire
                verts = obj.vertices().objects
                for vert in verts:
                    # Going over all vertices
                    # vert.wrapped gives us TopoDS_Vertex
                    pt = BRep_Tool.Pnt(vert.wrapped) # Convert vertex to Pnt
                    # Use self.intermediate, which is the workplane
                    # surf_anal = ShapeAnalysis_Surface()
                    # print("TODO", ShapeAnalysis_Surface.ValueOfUV(pt))
        elif self.mode == "VERT_ON":
            # The full object is given by self.intermediate
            vec = obj.objects[0].wrapped
            intermed = self.intermediate.objects[0].wrapped

            if type(vec) == OCC.Core.gp.gp_Vec:
                # Convert Vector to Point
                pnt = gp_Pnt()
                pnt.SetXYZ(vec.XYZ())
                #print("Using prog_rep Vector with values", pnt.X(), pnt.Y(), pnt.Z())
                # Flag that stores whether the point is on one of the faces
                tmp_flag = False
                # Go over all the faces in the intermediary object base.objects[0].wrapped
                aFaceExplorer = TopExp_Explorer(intermed, TopAbs_FACE)
                while aFaceExplorer.More():
                    aFace = topods.Face(aFaceExplorer.Current())
                    srf = BRep_Tool().Surface(aFace)
                    sas = ShapeAnalysis_Surface(srf)
                    face_e = BRepClass_FaceExplorer(aFace)
                    classifier = BRepClass_FClassifier()
                    p = sas.ValueOfUV(pnt, 0.001)
                    classifier.Perform(face_e, p, 0.001)
                    #print("Value of classifier", classifier.State())
                    if classifier.State() == 0:
                        tmp_flag = True
                        break
                    aFaceExplorer.Next()

                # The point must lie on one of the faces
                assert tmp_flag is True

            else:
                print("Warning! Unsupported type of obj supplied for checking.")
        if final:
            self.handle_success()

    def pretty_print_sorted_stats(self):
        """
        Pretty print stats of the sorted list of predicate results
        """
        temp_str = ""
        for r in self.sorted_pred_results.keys():
            temp_str = temp_str + str(r) + " has " + str(len(self.sorted_pred_results[r])) + " entries; "
        return temp_str

    def fin_it(self):
        """
        This method is called at the end of each iteration in the sampling procedure.
        Here, we consolidate the restrictions encountered during the sample iteration.
        :return: True when done, False, or something else if not
        """
        print("---End of iteration---")
        # Append the count of passing iterations
        if self.curr_it_pass:
            self.iter_passing = self.iter_passing + 1

        if self.analyze_mode is not None:
            if self.iter_num >= self.max_samples: return True
            else: return False

        # Depending on the mode we are using, update things accordingly
        elif self.sort_mode_active and self.state_svm is None:
            # Basic sort mode
            self.fin_it_sort()
            # Check whether to end
            return self.sort_check_end()

        elif self.state_onebyone is not None:
            return self.state_onebyone.fin_it()

        elif self.state_svm is not None and not self.sort_mode_active:
            # SVM mode
            return self.state_svm.fin_it()

        elif self.sort_mode_active and self.state_svm is not None:
            # Hybrid mode
            self.fin_it_sort()
            ret1 = self.sort_check_end()
            ret2 = self.state_svm.fin_it()
            if ret1 and ret2:
                return True
            else:
                return False

        elif self.state_lp is not None:
            self.state_lp.fin_it_lp()
            return self.state_lp.check_end()

        else:
            # Some legacy mode
            return self.sort_check_end()

    def fin_it_sort(self):
        """
        Moved from earlier within the fin_it() method, includes relevant structures for the sorted mode
        """
        # For each restriction collected in this iteration (not the one-by-one mode)
        for r in self.restrictions_curr:
            # Specially check if 'r' has been removed: ignore if so.
            print("----->Considering", r)

            if self.sort_mode_active:
                # if sort mode used, call a different method that updates the structure
                self.sorted_pred_results, self.removed_restr = constraints.update_sorted_predicates(
                    self.sorted_pred_results, r, self.removed_restr)

            else:
                if r not in self.removed_restr:
                    # If not using the sort mode (extended predicates added and then sorted)
                    self.restrictions_consolidated_may = \
                        constraints.score_restrictions(self.restrictions_consolidated_may, r)
                    self.restrictions_consolidated_must, self.restrictions_consolidated_may, self.removed_restr = \
                        constraints.update_restrictions(self.restrictions_consolidated_must,
                                                        self.restrictions_consolidated_may, self.removed_restr)

        # Set if this iteration passed / failed for overall tracking
        self.result_all.append(self.curr_it_pass)

        if not self.sort_mode_active:
            print("May restrictions", str(self.restrictions_consolidated_may))
        else:
            self.sorted_pred_results, self.removed_restr, self.restrictions_consolidated_must = \
                constraints.update_sorted_must(self.sorted_pred_results, self.removed_restr,
                                               self.restrictions_consolidated_must)
            print("Sorted predicate stats:", self.pretty_print_sorted_stats())
        print("Must restrictions", str(self.restrictions_consolidated_must))

    def sort_check_end(self):
        """
        Checks if it is time to stop sampling (relevant to sort modes)
        :return: True if end, False otherwise
        """
        # If there are no restrictions left in the temporary checking structure we use.
        if len(self.sorted_pred_results.keys()) == 0 or len(self.restrictions_consolidated_must.keys()) == self.num_vars:
            print("Time to end sort based synthesis!")
            return True
        else:
            return False

    def fin_all(self):
        """
        Called just before all kinds of analyses are about to end.
        """
        # Set all may restrictions to must restrictions
        """
        print("Must restrictions", str(self.restrictions_consolidated_must))
        print("May restrictions", str(self.restrictions_consolidated_may))
        """
        print("End of sampling\n-----")
        info = "\nTime taken:\t" + str(time.time() - self.time_start)
        if self.analyze_mode is not None:  # is 0 or 1, None means the mode is not active
            info = info + "\nSampling success rate (%):\t" + str((self.iter_passing/self.iter_num) * 100)
        if self.out_file is None:
            print(info)
        else:
            self.out_file.write(info)

    def change_mode(self, mode, arg_nums=[], prev=None, intermediate=None, alt=False):
        # TODO: Shell also causes an offet. Add this.
        # Do some checks on the previous object, if there is one
        if prev is not None and intermediate is None:
            self.check(prev)  # Check the previous object
            # No failure found, the previous step was a success
            self.handle_success()
            self.prev_obj = prev  # Set this to in the internal var, for possible future use.

        elif prev is not None and intermediate is not None:
            # The intermediate object, if given (usually the prev object if it is a vertex).
            self.intermediate = intermediate
            self.check(prev)  # Check the previous object
            # No failure found, the previous step was a success
            self.handle_success()
            self.prev_obj = intermediate  # This is the intermediate because we will use it to get BB info

        self.mode = mode.upper()

        # If doing non-stateful random sampling, just stop now.
        if self.analyze_mode == 0: return

        # If there is an alternative object being designed, save the vars separately
        if alt: print("Note that only CREATE, 2D, and extrude are supported in alt mode. Only LP based constraint synthesis!")
        self.alt_mode = alt

        if mode == "CREATE":
            # print("Create mode active...")
            # Update vars in constraints
            if not self.alt_mode:
                constraints.args_create = constraints.args_create + arg_nums
                # Update MUST dict with vars used in CREATE
                self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                                       constraints.find_restriction_dict_unb(arg_nums))
            else:
                constraints.args_alt = constraints.args_alt + arg_nums

        elif mode == "2D":
            # TODO: When does an arg become an offset arg? As this is not amply clear, we assume that 2D
            #  creates are unbounded.
            if not self.alt_mode:
                constraints.args_create = constraints.args_create + arg_nums
                self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                                    constraints.find_restriction_dict_unb(arg_nums))
                # self.add_unresolved(arg_nums)
            else:
                constraints.args_alt = constraints.args_alt + arg_nums

        elif mode == "UNION":
            self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                               constraints.find_restriction_dict_unb(
                                                                                   constraints.args_alt))
            constraints.args_alt = []

        elif mode == "2D_CUT":
            # The 2D_CUT mode
            constraints.args_cut = constraints.args_cut + arg_nums
            self.add_unresolved(arg_nums)

        elif mode == "OFFSET":  # Used to create an offsetted workplane
            # Append this to args that may be unbounded
            constraints.args_offset = constraints.args_offset + arg_nums
            self.add_unresolved(arg_nums)

        elif mode == "SHELL":
            # TODO: Shell needs to be handled differently. It fails when negative values are used, but this can't happen
            #  in current setup. Use the - sign in the program.
            constraints.args_offset = constraints.args_offset + arg_nums

        elif mode == "SHELL_PVE":
            print("Warning! Using Shell positive mode. There is no error checking expected, or handled. Temporary workaround for nn-robust shell")
            self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                               constraints.find_restriction_dict_unb(
                                                                                   arg_nums))

        elif mode == "EXTRUDE":
            if not self.alt_mode:
                # TODO: Is the extrude height relevant as a member of `constraints.args_create'?
                self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                                   constraints.find_restriction_dict_unb(arg_nums))
                constraints.args_create = constraints.args_create + arg_nums
            else:
                constraints.args_alt = constraints.args_alt + arg_nums

        elif mode == "LOFT":
            # TODO: Make this safer and better.
            # Assume that there is an offset earlier and just make this unbounded.
            self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                               constraints.find_restriction_dict_unb([constraints.args_offset[-1]]))

        elif mode == "HOLE":
            # Bounding box
            bb = self.prev_obj.objects[0].BoundingBox()
            bb_diag = bb.DiagonalLength

            # print("Bore hole mode active...")
            # Only 1 or 2 args allowed
            assert 1 <= len(arg_nums) <= 2

            # Restrict the diameter of the hole if not already the case; only do it if depth is not an argument
            if len(arg_nums) == 1:
                self.check_update_var(arg_pos=int(arg_nums[0]), max_val=bb_diag)
            elif len(arg_nums) == 2:
                self.check_update_var(arg_pos=int(arg_nums[0]), max_val=bb_diag)
                # The hole depth we use is at least the bounding box diagonal to restrict as much as possible.
                if self.analyze_mode is None:
                    self.check_update_var(arg_pos=int(arg_nums[1]), min_val=bb_diag)
                # The hole depth is unbounded
                self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                                   constraints.find_restriction_dict_unb(
                                                                                       [arg_nums[1]]))
            constraints.args_cut = constraints.args_cut + arg_nums

        elif mode == "CBORE":
            # Method: cboreHole(self, diameter, cboreDiameter, cboreDepth, depth=None, clean=True)
            # Bounding box
            bb = self.prev_obj.objects[0].BoundingBox()
            bb_diag = bb.DiagonalLength

            # print("Bore hole mode active...")
            # 3 args excluding depthW
            assert 3 <= len(arg_nums) <= 4
            # cbore diameter more than the hole diameter
            # Set the hole diameter < cbore diameter
            self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                               constraints.find_restriction_dict_lt(src=arg_nums[0], dest=arg_nums[1],
                                                                                                                    src_val=self.args[int(arg_nums[0])],
                                                                                                                    dest_val=self.args[int(arg_nums[1])]))

            if len(arg_nums) >= 3:
                # Restrict the diameter of the hole if not already the case
                self.check_update_var(arg_pos=int(arg_nums[0]), max_val=bb_diag)
                self.check_update_var(arg_pos=int(arg_nums[2]), max_val=bb_diag)

            if len(arg_nums) == 4:
                # Add additional constraint cbore_depth < depth
                self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                                   constraints.find_restriction_dict_lt(
                                                                                       src=arg_nums[2],
                                                                                       dest=arg_nums[3],
                                                                                       src_val=self.args[
                                                                                           int(arg_nums[2])],
                                                                                       dest_val=self.args[
                                                                                           int(arg_nums[3])]))
                # The hole depth we use is at least the bounding box diagonal to restrict as much as possible.
                if self.analyze_mode is None:
                    self.check_update_var(arg_pos=int(arg_nums[3]), min_val=bb_diag)
                # The hole depth is unbounded
                self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                                   constraints.find_restriction_dict_unb(
                                                                                       [arg_nums[3]]))

        elif mode == "CSKHOLE":
            # Method: cskHole(self, diameter, cskDiameter, cskAngle, depth=None, clean=True)
            # print("Countersink hole mode active...")
            # Bounding box
            bb = self.prev_obj.objects[0].BoundingBox()
            bb_diag = bb.DiagonalLength

            assert len(arg_nums) == 3 or len(arg_nums) == 4
            # Set the hole diameter < csk diameter
            self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                               constraints.find_restriction_dict_lt(src=arg_nums[0], dest=arg_nums[1],
                                                                                                                    src_val=self.args[int(arg_nums[0])],
                                                                                                                    dest_val=self.args[int(arg_nums[1])]))
            print("Warning! Not yet fully developed.")
            # Restrict the diameter of the hole if not already the case
            # self.check_update_var(arg_pos=[int(arg_nums[0])], max=bb_diag)

        elif mode == "FILLET":
            # Bounding box
            bb = self.prev_obj.objects[0].BoundingBox()
            bb_diag = bb.DiagonalLength

            assert len(arg_nums) == 1  # Only expecting the fillet radius to be passed
            # The radius of the fillet should be less than the diagonal of the bounding box of the object
            self.check_update_var(arg_pos=int(arg_nums[0]), max_val=bb_diag / 2)

        elif mode == "CHAMFER":
            # Bounding box
            bb = self.prev_obj.objects[0].BoundingBox()
            bb_diag = bb.DiagonalLength

            assert len(arg_nums) == 1  # Only expecting the fillet radius to be passed
            # The radius of the fillet should be less than the diagonal of the bounding box of the object
            self.check_update_var(arg_pos=int(arg_nums[0]), max_val=bb_diag / 2)

        # No matter what, add the current indices to the list of activated indices.
        if len(arg_nums) != 0:
            self.arg_indices_activated.append(arg_nums)
            self.arg_indices_activated_flat = list(set(self.arg_indices_activated_flat + arg_nums))

    def handle_exception(self, exc):
        """
        Handles exceptions caused by improper runs and creates restrictions based
        on the current state of the program.
        :return:
        """

        print("Failure at " + self.mode + ", argrs: ", self.args, "exception", exc, "traceback", traceback.format_exc())
        self.curr_it_pass = False
        if self.analyze_mode is not None: return

        # If using the stateful svm mode, it is useful to add this information
        if self.state_svm is not None:
            self.state_svm.new_training_entry()

        if self.mode == "HOLE":
            # print("Handling bore hole...")
            if self.state_lp is not None:
                # We pass for_args as we need not consider the depth in our scheme
                self.state_lp.add_hypothesis_sample(result=False, for_args=[self.arg_indices_activated[-1][0]])
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=False)

        elif self.mode == "EXTRUDE":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=False, use_prev=True)
                # Now we can clear intermediary args
                constraints.args_offset = []
                constraints.args_cut = []
            else:
                pass

        elif self.mode == "CUT":
            if not self.alt_mode:
                if self.state_lp is not None:
                    self.state_lp.add_hypothesis_sample(result=False)
                    # Now we can clear intermediary args
                    constraints.args_offset = []
                    constraints.args_cut = []
                else:
                    self.restrictions_curr = self.restrictions_curr + \
                                             constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                              removed_restr=self.removed_restr, result=False)
            else:
                # If LP mode
                if self.state_lp is not None:
                    self.state_lp.add_hypothesis_sample(result=False, for_args=constraints.args_alt)
                    # Now we can clear intermediary args
                    constraints.args_offset = []
                    constraints.args_cut = []
                    constraints.args_alt = []

        elif self.mode == "SHELL":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=False)
                # Now we can clear intermediary args
                constraints.args_offset = []
                constraints.args_cut = []
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=False)

        elif self.mode == "VERT_ON":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=False)

        elif self.mode == "LOFT":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=False, use_prev=True)
                # Now we can clear intermediary args
                constraints.args_offset = []
                constraints.args_cut = []
            else:
                pass

        elif self.mode == "CBORE":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=False)
            else:
                # We can ignore the argument at arg_nums[1]
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=False,
                                                                          ignore_src=[self.arg_indices_activated[-1][1]])

        elif self.mode == "CSKHOLE":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=False)
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=False,
                                                                          ignore_src=[self.arg_indices_activated[-1][1]])

        elif self.mode == "FILLET":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=False)
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=False)

        elif self.mode == "CHAMFER":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=False)
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=False)

    def handle_success(self):
        print("Success at " + self.mode + ", argrs: ", self.args)
        if self.analyze_mode is not None: return

        # If using the stateful svm mode, it is useful to add this information
        if self.state_svm is not None:
            self.state_svm.new_training_entry()

        if self.mode == "HOLE":
            # print("Handling bore hole...")
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=True, for_args=[self.arg_indices_activated[-1][0]])
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=True)

        elif self.mode == "EXTRUDE":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=True, use_prev=True)
                # Now we can clear intermediary args
                constraints.args_offset = []
                constraints.args_cut = []
            else:
                pass

        elif self.mode == "CUT" or self.mode == "SHELL":
            if not self.alt_mode:
                if self.state_lp is not None:
                    self.state_lp.add_hypothesis_sample(result=True)
                    # Now we can clear intermediary args
                    constraints.args_offset = []
                    constraints.args_cut = []
                else:
                    self.restrictions_curr = self.restrictions_curr + \
                                             constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                              removed_restr=self.removed_restr, result=True)
            else:
                # If LP mode
                if self.state_lp is not None:
                    self.state_lp.add_hypothesis_sample(result=True, for_args=constraints.args_alt)
                    # Now we can clear intermediary args
                    constraints.args_offset = []
                    constraints.args_cut = []
                    constraints.args_alt = []

        elif self.mode == "VERT_ON":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=True)

        elif self.mode == "LOFT":
            # A loft is typically successful.
            self.restrictions_consolidated_must = constraints.update_must_dict(self.restrictions_consolidated_must,
                                                                               constraints.find_restriction_dict_unb(
                                                                                   self.unresolved_args))
            # Now we can clear intermediary args
            constraints.args_offset = []
            constraints.args_cut = []

            self.clear_unresolved()

        elif self.mode == "CBORE":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=True)
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=True,
                                                                          ignore_src=[self.arg_indices_activated[-1][1]])

        elif self.mode == "CSKHOLE":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=True)
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=True,
                                                                          ignore_src=[self.arg_indices_activated[-1][1]])

        elif self.mode == "FILLET":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=True)
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=True)

        elif self.mode == "CHAMFER":
            if self.state_lp is not None:
                self.state_lp.add_hypothesis_sample(result=True)
            else:
                self.restrictions_curr = self.restrictions_curr + \
                                         constraints.find_restriction_cut(self.arg_indices_activated, self.args,
                                                                          removed_restr=self.removed_restr, result=True)

    def check_update_var(self, arg_pos, max_val=constraints.inf, min_val=constraints.low_val):
        """
        :param arg_pos: The indices that need to be constrained :type list of ints
        :param max_val: The max value possible
        :param min_val: The min value possible
        :return: No return, directly change self.args
        """

        """
        l = len(arg_pos)
        for i in range(0, l):
            # If there is one element in the list which is less than max_val, we can return
            if self.args[arg_pos[i]] < max_val: return
        # If the function did not return, then change the value for a randomly chosen index
        pos_change = random.randint(0, l-1)
        old = self.args[arg_pos[pos_change]]
        self.args[arg_pos[pos_change]] = random.uniform(constraints.low_val, max_val)
        print("Readjusting variable value", old, "to", self.args[arg_pos[pos_change]])
        """
        if self.analyze_mode != 0:
            # If not in analyze mode, then readjust
            if self.args[arg_pos] > max_val or self.args[arg_pos] < min_val:
                old = self.args[arg_pos]
                self.args[arg_pos] = random.uniform(min_val, max_val)
                print("Readjusting variable value", old, "to", self.args[arg_pos])

    def clear_unresolved(self):
        """
        Clear the unresolved list
        :return:
        """
        self.unresolved_args = []

    def add_unresolved(self, args):
        """
        Add arguments to the unresolved list.
        :param args:
        :return:
        """
        self.unresolved_args = self.unresolved_args + args


class state_onebyone:
    """
    Data structure to hold state of teh one-by-one sampling technique
    """
    def __init__(self, p_rep):
        # Assume that if the class is initialized, this mode is active
        self.active = True
        # Reference to the prog_rep
        self.p_rep = p_rep

        # The list of params that have already been tested
        self.params_done = []
        # The current param under test (start by default with 0)
        self.param_curr = 0

        # Passing, failing samples (type: list of lists)
        self.passing_vars = []
        self.failing_vars = []

    def new_sample(self):
        """
        Generate a new sample and uupdate internal vars.
        """
        # Reset the list we will build
        self.p_rep.args = []
        # If there are no passing var values available.
        if not self.passing_vars:
            # There is nothing known as yet about passing cases. Sample until pass.
            print("(Singular Mode) Trying initial values.")
            for i in range(self.p_rep.num_vars):
                self.p_rep.args.append(random.uniform(constraints.low_val, constraints.inf))
        else:
            # There is already a starting point which needs to be perturbed\
            print("(Singular Mode) Trying perturbation on", self.param_curr)
            for i in range(self.p_rep.num_vars):
                if i == self.param_curr:
                    # Set a random value to the chosen argument no.
                    self.p_rep.args.append(random.uniform(constraints.low_val, constraints.inf))
                else:
                    # Set fixed values to all NOT CHOSEN argument no.s
                    self.p_rep.args.append(self.passing_vars[0][i])

    def update(self):
        # Use the state keeper depending on the restrictions obtained so far.
        print("Updating state_onebyone")
        # Depending on if the iteration passed or failed, add the relevant argument values to the pass / fail lists
        if self.p_rep.curr_it_pass:
            self.passing_vars.append(self.p_rep.args)
        else:
            self.failing_vars.append(self.p_rep.args)

        # Manage the restrictions encountered in this iteration

        temp_no_curr_var = True
        # For each restriction collected in this iteration (not the one-by-one mode)
        for r in self.p_rep.restrictions_curr:
            # Specially check if 'r' has been removed: ignore if so.
            print("----->Considering", r)
            if r.dest == str(self.param_curr):
                temp_no_curr_var = False
            # if sort mode used, call a different method that updates the structure
            self.p_rep.sorted_pred_results, self.p_rep.removed_restr = constraints.update_sorted_predicates(
                self.p_rep.sorted_pred_results, r, self.p_rep.removed_restr)

        # Set if this iteration passed / failed for overall tracking
        self.p_rep.result_all.append(self.p_rep.curr_it_pass)

        self.p_rep.sorted_pred_results, self.p_rep.removed_restr, self.p_rep.restrictions_consolidated_must = \
            constraints.update_sorted_must(self.p_rep.sorted_pred_results, self.p_rep.removed_restr,
                                           self.p_rep.restrictions_consolidated_must)
        print("Sorted predicate stats:", self.p_rep.pretty_print_sorted_stats())
        print("Must restrictions", str(self.p_rep.restrictions_consolidated_must))

        # Move ahead with the singular var to be perturbed in the next iteration
        if temp_no_curr_var:
            if self.param_curr < self.p_rep.num_vars - 1:
                self.param_curr = self.param_curr + 1
            else:
                print("The end!")

