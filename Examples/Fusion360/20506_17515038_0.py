# Name: 20506_17515038_0 Number of arguments: 6
# Taken from Fusion 360 Segmentation dataset

import cadquery as cq
print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="2D", arg_nums=["0"])
    base = cq.Workplane("XY").circle(prog_rep.args[0])

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["1"], prev=base)
    base = base.extrude(prog_rep.args[1])

    prog_rep.change_mode(mode="CHAMFER", arg_nums=["2"], prev=base)
    base = base.edges(">Z").chamfer(prog_rep.args[2])

    prog_rep.change_mode(mode="HOLE", arg_nums=["3"], prev=base)
    base = base.faces(">Z").hole(prog_rep.args[3])

    # A hole with an offset
    prog_rep.change_mode(mode="VERT_ON", arg_nums=["4"], prev=base)
    base2 = base.faces(">Z").workplane().transformed(offset=(prog_rep.args[4], 0, 0))

    # Make the hole in the selected vertices (we use intermediate as prev does not take vertices))
    prog_rep.change_mode(mode="HOLE", arg_nums=["5"], intermediate=base, prev=base2)
    base2 = base2.hole(prog_rep.args[5])

    return base2