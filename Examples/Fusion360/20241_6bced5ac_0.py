# Name: 20241_6bced5ac_0 Number of arguments: 8
# Taken from Fusion 360 Segmentation dataset


import cadquery as cq

print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    base = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    # Get vertex where the cut is going to be made (Note: faces(">Z") is not working)
    prog_rep.change_mode(mode="VERT_ON", arg_nums=["3"], prev=base)
    base2 = cq.Workplane("XY").transformed(offset=(prog_rep.args[3], 0, prog_rep.args[2]/2))

    # alt is set to True because we are working on another intermediate object.
    prog_rep.change_mode(mode="CREATE", arg_nums=["4", "5", "6"], intermediate=base, prev=base2, alt=True)
    base2 = base2.box(prog_rep.args[4], prog_rep.args[5], prog_rep.args[6], combine=False)

    prog_rep.change_mode(mode="FILLET", arg_nums=["7"], prev=base2, alt=True)
    base2 = base2.edges("|Z").fillet(prog_rep.args[7])

    prog_rep.change_mode(mode="CUT", prev=base2, alt=True)
    base = base.cut(base2)

    return base