# Name: 20163_3742f394_2 Number of arguments: 12
# Taken from Fusion 360 Segmentation dataset

import cadquery as cq
print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="2D", arg_nums=["0"])
    base = cq.Workplane("XY").circle(prog_rep.args[0]/2)

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["1"], prev=base)
    base = base.extrude(prog_rep.args[1])

    prog_rep.change_mode(mode="VERT_ON", arg_nums=["2"], prev=base)
    base2 = base.faces(">Z").workplane().transformed(offset=(prog_rep.args[2], 0, 0))

    prog_rep.change_mode(mode="CBORE", arg_nums=["3", "4", "5"], intermediate=base, prev=base2)
    base2 = base2.cboreHole(prog_rep.args[3], prog_rep.args[4], prog_rep.args[5])

    prog_rep.change_mode(mode="2D", arg_nums=["6"], prev=base2)
    base2 = base2.faces(">Z").circle(prog_rep.args[6]/2)

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["7"], prev=base2)
    base2 = base2.extrude(prog_rep.args[7])

    prog_rep.change_mode(mode="FILLET", arg_nums=["8"], prev=base2)
    base2 = base2.edges(">Z").fillet(prog_rep.args[8])

    prog_rep.change_mode(mode="2D", arg_nums=["9"], prev=base2)
    base2 = base2.faces(">Z").circle(prog_rep.args[9]/2)

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["10"], prev=base2)
    base2 = base2.extrude(prog_rep.args[10])

    prog_rep.change_mode(mode="FILLET", arg_nums=["11"], prev=base2)
    base2 = base2.edges(">Z").fillet(prog_rep.args[11])

    return base2