# Name: 20443_33840975_0 Number of arguments: 4
# Taken from Fusion 360 Segmentation dataset


import cadquery as cq
print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="2D", arg_nums=["0"])
    base = cq.Workplane("XY").circle(prog_rep.args[0]/2)

    prog_rep.change_mode(mode="OFFSET", arg_nums=["1"], prev=base)
    base = base.workplane(prog_rep.args[1])

    prog_rep.change_mode(mode="2D", arg_nums=["2"], prev=base)
    base = base.circle(prog_rep.args[2]/2)

    prog_rep.change_mode(mode="LOFT", prev=base)
    base = base.loft(combine=True)

    prog_rep.change_mode(mode="FILLET", arg_nums=["3"], prev=base)
    base = base.edges(">Z").fillet(prog_rep.args[3])

    return base
