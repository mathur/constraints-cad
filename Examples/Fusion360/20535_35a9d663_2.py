# Name: 20535_35a9d663_2 Number of arguments: 11
# Taken from Fusion 360 Segmentation dataset

import cadquery as cq
print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="2D", arg_nums=["0"])
    base = cq.Workplane("XY").circle(prog_rep.args[0])

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["1"], prev=base)
    base = base.extrude(prog_rep.args[1])

    prog_rep.change_mode(mode="CHAMFER", arg_nums=["2"], prev=base)
    base = base.edges("%CIRCLE").chamfer(prog_rep.args[2])

    prog_rep.change_mode(mode="2D", arg_nums=["3"], prev=base)
    base = base.faces(">Z").circle(prog_rep.args[3])

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["4"], prev=base)
    base = base.extrude(prog_rep.args[4])

    prog_rep.change_mode(mode="2D", arg_nums=["5"], prev=base)
    base = base.faces(">Z").polygon(nSides=6, diameter=prog_rep.args[5])

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["6"], prev=base)
    base = base.extrude(prog_rep.args[6])

    prog_rep.change_mode(mode="2D", arg_nums=["7"], prev=base)
    base = base.faces(">Z").circle(prog_rep.args[7])

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["8"], prev=base)
    base = base.extrude(prog_rep.args[8])

    # Hole with parameterized depth.
    prog_rep.change_mode(mode="HOLE", arg_nums=["9", "10"], prev=base)
    base = base.faces(">Z").hole(prog_rep.args[9], prog_rep.args[10])

    return base