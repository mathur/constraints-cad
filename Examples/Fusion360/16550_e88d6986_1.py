# Name: 16550_e88d6986_1 Number of arguments: 4
# Taken from Fusion 360 Segmentation dataset

import cadquery as cq
print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="2D", arg_nums=["0"])
    base = cq.Workplane("XY").polygon(nSides=6, diameter=prog_rep.args[0])

    prog_rep.change_mode(mode="EXTRUDE", arg_nums=["1"], prev=base)
    base = base.extrude(prog_rep.args[1])

    prog_rep.change_mode(mode="CHAMFER", arg_nums=["2"], prev=base)
    base = base.edges(">Z").chamfer(prog_rep.args[2])

    prog_rep.change_mode(mode="HOLE", arg_nums=["3"], prev=base)
    base = base.faces(">Z").hole(prog_rep.args[3])

    return base