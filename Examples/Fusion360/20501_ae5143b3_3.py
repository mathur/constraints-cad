# Name: 20501_ae5143b3_3 Number of arguments: 7
# Taken from Fusion 360 Segmentation dataset

import cadquery as cq
print("----", "Using", __file__, "-----")


def main(prog_rep=None):
    """
    The main structure which creates the design
    :param args: The arguments to the design in the form of a list
    :return:
    """

    prog_rep.change_mode(mode="CREATE", arg_nums=["0", "1", "2"])
    base = cq.Workplane("XY").box(prog_rep.args[0], prog_rep.args[1], prog_rep.args[2])

    prog_rep.change_mode(mode="FILLET", arg_nums=["3"], prev=base)
    base = base.edges("|Z").fillet(prog_rep.args[3])

    prog_rep.change_mode(mode="HOLE", arg_nums=["4"], prev=base)
    base = base.faces(">Z").hole(prog_rep.args[4])

    # Two more holes equidistant from the centre
    prog_rep.change_mode(mode="VERT_ON", arg_nums=["5"], prev=base)
    base2 = base.edges(">Y and >Z").workplane().transformed(offset=(0, -prog_rep.args[5], 0))

    # Make the hole in the selected vertices (we use intermediate as prev does not take vertices))
    prog_rep.change_mode(mode="HOLE", arg_nums=["6"], intermediate=base, prev=base2)
    base2 = base2.hole(prog_rep.args[6])
    base2 = base2.edges("<Y and >Z").workplane().transformed(offset=(0, prog_rep.args[5], 0)).hole(prog_rep.args[6])

    return base2